<div class='wrap oath-settings'>
	<h2>Wordpress OAuth Settings</h2>
	<!-- START Settings Header -->
	<div id="oath-settings-header"></div>
	<!-- END Settings Header -->
	<!-- START Settings Body -->
	<div id="oath-settings-body">
		<!-- START Settings Column 1 -->
		<div id="oath-settings-col1" class="oath-settings-column">
			<form method='post' action='options.php'>
				<?php settings_fields( 'oath_settings' ); ?>
				<?php do_settings_sections( 'oath_settings' ); ?>

				<!-- START Login with Google section -->
				<div id="oath-settings-section-login-with-google" class="oath-settings-section">
					<h3>Login with Google</h3>
					<div class='form-padding'>
						<table class='form-table'>
							<tr valign='top'>
								<th scope='row'>Enabled:</th>
								<td>
									<input type='checkbox' name='oath_google_api_enabled'
									       value='1' <?php checked( get_option( 'oath_google_api_enabled' ) == 1 ); ?> />
								</td>
							</tr>

							<tr valign='top'>
								<th scope='row'>Client ID:</th>
								<td>
									<input type='text' name='oath_google_api_id'
									       value='<?php echo get_option( 'oath_google_api_id' ); ?>'/>
								</td>
							</tr>

							<tr valign='top'>
								<th scope='row'>Client Secret:</th>
								<td>
									<input type='text' name='oath_google_api_secret'
									       value='<?php echo get_option( 'oath_google_api_secret' ); ?>'/>
								</td>
							</tr>
						</table> <!-- .form-table -->
						<p>
							<strong>Instructions:</strong>
						<ol>
							<li>Visit the Google website for developers <a
									href='https://console.developers.google.com/project' target="_blank">console.developers.google.com</a>.
							</li>
							<li>At Google, create a new Project and enable the Google+ API. This will enable your site
								to access the Google+ API.
							</li>
							<li>At Google, provide your site's homepage URL (<?php echo $blog_url; ?>) for the new
								Project's Redirect URI. Don't forget the trailing slash!
							</li>
							<li>At Google, you must also configure the Consent Screen with your Email Address and
								Product Name. This is what Google will display to users when they are asked to grant
								access to your site/app.
							</li>
							<li>Paste your Client ID/Secret provided by Google into the fields above, then click the
								Save all settings button.
							</li>
						</ol>
						</p>
						<?php submit_button( 'Save all settings' ); ?>
					</div> <!-- .form-padding -->
				</div> <!-- .oath-settings-section -->
				<!-- END Login with Google section -->

				<!-- START Login with Facebook section -->
				<div id="oath-settings-section-login-with-facebook" class="oath-settings-section">
					<h3>Login with Facebook</h3>
					<div class='form-padding'>
						<table class='form-table'>
							<tr valign='top'>
								<th scope='row'>Enabled:</th>
								<td>
									<input type='checkbox' name='oath_facebook_api_enabled'
									       value='1' <?php checked( get_option( 'oath_facebook_api_enabled' ) == 1 ); ?> />
								</td>
							</tr>

							<tr valign='top'>
								<th scope='row'>App ID:</th>
								<td>
									<input type='text' name='oath_facebook_api_id'
									       value='<?php echo get_option( 'oath_facebook_api_id' ); ?>'/>
								</td>
							</tr>

							<tr valign='top'>
								<th scope='row'>App Secret:</th>
								<td>
									<input type='text' name='oath_facebook_api_secret'
									       value='<?php echo get_option( 'oath_facebook_api_secret' ); ?>'/>
								</td>
							</tr>
						</table> <!-- .form-table -->
						<p>
							<strong>Instructions:</strong>
						<ol>
							<li>Register as a Facebook Developer at <a href='https://developers.facebook.com/'
							                                           target="_blank">developers.facebook.com</a>.
							</li>
							<li>At Facebook, create a new App. This will enable your site to access the Facebook API.
							</li>
							<li>At Facebook, provide your site's homepage URL (<?php echo $blog_url; ?>) for the new
								App's Redirect URI. Don't forget the trailing slash!
							</li>
							<li>Paste your App ID/Secret provided by Facebook into the fields above, then click the Save
								all settings button.
							</li>
						</ol>
						</p>
						<?php submit_button( 'Save all settings' ); ?>
					</div> <!-- .form-padding -->
				</div> <!-- .oath-settings-section -->
				<!-- END Login with Facebook section -->

				<!-- START Login with LinkedIn section -->
				<div id="oath-settings-section-login-with-linkedin" class="oath-settings-section">
					<h3>Login with LinkedIn</h3>
					<div class='form-padding'>
						<table class='form-table'>
							<tr valign='top'>
								<th scope='row'>Enabled:</th>
								<td>
									<input type='checkbox' name='oath_linkedin_api_enabled'
									       value='1' <?php checked( get_option( 'oath_linkedin_api_enabled' ) == 1 ); ?> />
								</td>
							</tr>

							<tr valign='top'>
								<th scope='row'>API Key:</th>
								<td>
									<input type='text' name='oath_linkedin_api_id'
									       value='<?php echo get_option( 'oath_linkedin_api_id' ); ?>'/>
								</td>
							</tr>

							<tr valign='top'>
								<th scope='row'>Secret Key:</th>
								<td>
									<input type='text' name='oath_linkedin_api_secret'
									       value='<?php echo get_option( 'oath_linkedin_api_secret' ); ?>'/>
								</td>
							</tr>
						</table> <!-- .form-table -->
						<p>
							<strong>Instructions:</strong>
						<ol>
							<li>Register as a LinkedIn Developer at <a href='https://developers.linkedin.com/'
							                                           target="_blank">developers.linkedin.com</a>.
							</li>
							<li>At LinkedIn, create a new App. This will enable your site to access the LinkedIn API.
							</li>
							<li>At LinkedIn, provide your site's homepage URL (<?php echo $blog_url; ?>) for the new
								App's Redirect URI. Don't forget the trailing slash!
							</li>
							<li>Paste your API Key/Secret provided by LinkedIn into the fields above, then click the
								Save all settings button.
							</li>
						</ol>
						</p>
						<?php submit_button( 'Save all settings' ); ?>
					</div> <!-- .form-padding -->
				</div> <!-- .oath-settings-section -->
				<!-- END Login with LinkedIn section -->

				<!-- START Login with PayPal section -->
				<div id="oath-settings-section-login-with-paypal" class="oath-settings-section">
					<h3>Login with PayPal</h3>
					<div class='form-padding'>
						<table class='form-table'>
							<tr valign='top'>
								<th scope='row'>Enabled:</th>
								<td>
									<input type='checkbox' name='oath_paypal_api_enabled'
									       value='1' <?php checked( get_option( 'oath_paypal_api_enabled' ) == 1 ); ?> />
								</td>
							</tr>

							<tr valign='top'>
								<th scope='row'>Sandbox mode:</th>
								<td>
									<input type='checkbox' name='oath_paypal_api_sandbox_mode'
									       value='1' <?php checked( get_option( 'oath_paypal_api_sandbox_mode' ) == 1 ); ?> />
									<p class="tip-message">PayPal offers a sandbox mode for developers who wish to setup
										and test PayPal Login with their site before going live.</p>
								</td>
							</tr>

							<tr valign='top'>
								<th scope='row'>Client ID:</th>
								<td>
									<input type='text' name='oath_paypal_api_id'
									       value='<?php echo get_option( 'oath_paypal_api_id' ); ?>'/>
								</td>
							</tr>

							<tr valign='top'>
								<th scope='row'>Client Secret:</th>
								<td>
									<input type='text' name='oath_paypal_api_secret'
									       value='<?php echo get_option( 'oath_paypal_api_secret' ); ?>'/>
								</td>
							</tr>
						</table> <!-- .form-table -->
						<p>
							<strong>Instructions:</strong>
						<ol>
							<li>Register as a PayPal Developer at <a href='https://developer.paypal.com'
							                                         target="_blank">developer.paypal.com</a>.
							</li>
							<li>At PayPal, create a new App. This will enable your site to access the PayPal API. Your
								PayPal App will begin in <em>sandbox mode</em> for testing.
							</li>
							<li>At PayPal, provide your site's homepage URL (<?php echo $blog_url; ?>) for the <em>App
									redirect URLs</em>. Don't forget the trailing slash!
							</li>
							<li>At PayPal, in the APP CAPABILITIES section, enable <em>Log In with PayPal</em>.</li>
							<li>Paste your Client ID/Secret provided by PayPal into the fields above, then click the
								Save all settings button.
							</li>
							<li>After testing PayPal login in <em>sandbox mode</em> with your site, you'll eventually
								want to switch the App over to <em>live mode</em> at PayPal, and turn off the Sandbox
								mode above.
							</li>
						</ol>
						</p>
						<?php submit_button( 'Save all settings' ); ?>
					</div> <!-- .form-padding -->
				</div> <!-- .oath-settings-section -->
				<!-- END Login with PayPal section -->

				<!-- START Login with Instagram section -->
				<div id="oath-settings-section-login-with-instagram" class="oath-settings-section">
					<h3>Login with Instagram</h3>
					<div class='form-padding'>
						<table class='form-table'>
							<tr valign='top'>
								<th scope='row'>Enabled:</th>
								<td>
									<input type='checkbox' name='oath_instagram_api_enabled'
									       value='1' <?php checked( get_option( 'oath_instagram_api_enabled' ) == 1 ); ?> />
								</td>
							</tr>

							<tr valign='top'>
								<th scope='row'>Client ID:</th>
								<td>
									<input type='text' name='oath_instagram_api_id'
									       value='<?php echo get_option( 'oath_instagram_api_id' ); ?>'/>
								</td>
							</tr>

							<tr valign='top'>
								<th scope='row'>Client Secret:</th>
								<td>
									<input type='text' name='oath_instagram_api_secret'
									       value='<?php echo get_option( 'oath_instagram_api_secret' ); ?>'/>
								</td>
							</tr>
						</table> <!-- .form-table -->
						<p>
							<strong>Instructions:</strong>
						<ol>
							<li>NOTE: Instagram's developer signup requires a valid cell phone number.</li>
							<li>At Instagram, register as an <a href='http://instagram.com/developer/authentication/'
							                                    target="_blank">Instagram Developer</a>.
							</li>
							<li>At Instagram, after signing up/in, click <a
									href='http://instagram.com/developer/clients/manage/'>Manage Clients</a>.
							</li>
							<li>At Instagram, click <a href="http://instagram.com/developer/clients/register/">Register
									a New Client</a>. This will enable your site to access the Instagram API.
							</li>
							<li>At Instagram, provide your site's homepage URL (<?php echo $blog_url; ?>) for the <em>OAuth
									redirect_uri</em>. Don't forget the trailing slash!
							</li>
							<li>At Instagram, copy the <em>Client ID/Client Secret</em> provided by Instagram and paste
								them into the fields above, then click the Save all settings button.
							</li>
						</ol>
						<strong>References:</strong>
						<ul>
							<li><a href='http://instagram.com/developer/authentication/'>Instagram Developer Reference -
									Authentication</a></li>
						</ul>
						</p>
						<?php submit_button( 'Save all settings' ); ?>
					</div> <!-- .form-padding -->
				</div> <!-- .oath-settings-section -->
				<!-- END Login with Instagram section -->
			</form> <!-- form -->
		</div>
		<!-- END Settings Column 1 -->
	</div> <!-- #oath-settings-body -->
	<!-- END Settings Body -->
</div> <!-- .wrap .oath-settings -->